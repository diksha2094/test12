package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;
    int DISTANCE_FROM_RIGHT = 500;
    int DISTANCE_FROM_BOTTOM = 200;
    int cage_width = 400;
    int cage_height = 80;
    int min_distance_from_wall = 400;
    int cage_speed = 5;
    int cat_speed = 10;
    int bullet_speed = 30;
    int gameOver=0;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;


    Sprite player;
    Sprite sparrow;
    Sprite cat;


    Point cage;

    Point cat_Position;

    Rect cat_hitbox;

    Rect cage_hitbox;

    boolean cage_moving_left = true;
    boolean cat_moving_left = true;
    boolean cage_moving_down = true;
    boolean win=true;
    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score = 0;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), VISIBLE_RIGHT - DISTANCE_FROM_RIGHT, VISIBLE_BOTTOM - DISTANCE_FROM_BOTTOM, R.drawable.cat64);

        cage = new Point();
        this.cage.x = this.VISIBLE_LEFT + min_distance_from_wall;
        this.cage.y = this.VISIBLE_TOP;

        this.cat_Position = new Point();
        this.cat_Position.x = this.VISIBLE_RIGHT - DISTANCE_FROM_RIGHT;
        this.cat_Position.y = this.VISIBLE_BOTTOM - DISTANCE_FROM_BOTTOM;

        //initialize the hitbox for cat
        this.cat_hitbox = new Rect(this.cat.getxPosition(), this.cat.getyPosition(), this.cat.getxPosition() + this.cat.getImagewidth, this.cat.getyPosition() + this.cat.getimageHeigth);

        this.cage_hitbox = new Rect(this.cage.x, this.cage.y, this.cage.x + this.cage_width, this.cage.y + this.cage_height);

        //make the bullets
        this.bullets.add(new Square(context, 100, 500, SQUARE_WIDTH));
        this.bullets.add(new Square(context, 100, 700, SQUARE_WIDTH));


    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    // Game Loop methods
    public void updateGame() {

        //make the cat move
        if (cat_moving_left == true) {
            this.cat.setxPosition(this.cat.getxPosition() - cat_speed);
            this.cat.updateHitbox();


            Log.d(TAG, "Cat x" + cat_Position.x);
        } else {
            this.cat.setxPosition(this.cat.getxPosition() + cat_speed);
            this.cat.updateHitbox();
            Log.d(TAG, "Cat x" + cat_Position.x);

        }

        if (this.cat.getxPosition() < (this.VISIBLE_LEFT + min_distance_from_wall)) {
            cat_moving_left = false;
        }
        if (this.cat.getxPosition() > this.VISIBLE_RIGHT - min_distance_from_wall) {
            cat_moving_left = true;
        }


        //move the cage left and right

        if (cage_moving_left == true) {
            this.cage.x = this.cage.x - cage_speed;
        } else {
            this.cage.x = this.cage.x + cage_speed;
        }

        if (cage.x < (this.VISIBLE_LEFT + min_distance_from_wall)) {
            cage_moving_left = false;
        }
        if (cage.x > this.VISIBLE_RIGHT - min_distance_from_wall) {
            cage_moving_left = true;
        }


        if (cage_moving_down == true) {
            this.cage.y = this.cage.y - cage_speed;
        } else {
            this.cage.y = this.cage.y + cage_speed;
        }

        if (cage.y < (this.VISIBLE_BOTTOM - 300)) {
            cage_moving_down = false;
            if(cage_hitbox.intersect(cat_hitbox)){
                gameOver=1;

            }
            else{
                
            }


        }
    }









    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);

            //3. cat
            canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);



            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);

            // draw the hitbox on the cat
         Rect r1 = cat.getHitbox();
           paintbrush.setStyle(Paint.Style.STROKE);
          canvas.drawRect(r1, paintbrush);

            int hitbox_left=this.cat_Position.x;
            int hitbox_top=this.cat_Position.y;
            int hitbox_right=this.cat_Position.x+this.cat.getImagewidth;
            int hitbox_bottom=this.cat_Position.y+this.cat.getimageHeigth;
            canvas.drawRect(hitbox_left,hitbox_top,hitbox_right,hitbox_bottom,paintbrush);

            //draw the cage

            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setColor(Color.RED);
            int cage_left=this.cage.x;
            int cage_top=this.cage.y;
            int cage_right=this.cage.x+cage_width;
            int cage_bottom=this.cage.y+cage_height;
            canvas.drawRect(cage_left,cage_top,cage_right,cage_bottom,paintbrush);


            //draw hitbox for cage

            paintbrush.setColor(Color.GREEN);
            paintbrush.setStyle(Paint.Style.STROKE);
            int hitbox_left1=this.cage.x;
            int hitbox_top1=this.cage.y;
            int hitbox_right1=this.cage.x+this.cage_width;
            int hitbox_bottom1=this.cage.y+this.cage_height;
            canvas.drawRect(hitbox_left1,hitbox_top1,hitbox_right1,hitbox_bottom1,paintbrush);



            //draw all the bullets
            for(int i=0;i<this.bullets.size();i++){

                paintbrush.setColor(Color.YELLOW);
                paintbrush.setStyle(Paint.Style.FILL);
                Square b=this.bullets.get(i);
                int x=b.getxPosition();
                int y=b.getyPosition();

                canvas.drawRect(x,y,x+b.getWidth(),y+b.getWidth(),paintbrush);

                paintbrush.setColor(Color.GREEN);
                paintbrush.setStyle(Paint.Style.STROKE);
                canvas.drawRect(b.getHitbox(), paintbrush);
            }

            //draw bullet hitbox

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {



            if (event.getX() <= this.screenWidth  && event.getY()<=this.screenHeight) {
                Log.d(TAG, "Person clicked LEFT side");

                //make the bullets move
                for(int i=0;i<this.bullets.size();i++){
                    Square bull=this.bullets.get(i);


                    double a=this.cage.x-bull.getxPosition();
                    double b=this.cage.y-bull.getyPosition();
                    double d=Math.sqrt((a*a)+(b*b));
                    double xn=(a/d);
                    double yn=(b/d);

                    int newX=bull.getxPosition()+(int)(xn*30);
                    int newY=bull.getyPosition()+(int)(yn*30);

                    bull.setxPosition(newX);
                    bull.setyPosition(newY);
                    bull.updateHitbox();

                    if (bull.getHitbox().intersect(cage_hitbox)){
                        cage_moving_down=true;
                        cage_moving_left=false;
                    }
                }

            }


        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

